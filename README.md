# Eclipse Api Documentation

This project describes the resources that make up the official Eclipse Foundation REST API.

## Links

- [SwaggerUI](https://eclipsefdn.github.io/api.eclipse.org-docs/swagger-ui/)
- OpenAPI Raw Files: [JSON](https://eclipsefdn.github.io/api.eclipse.org-docs/openapi.json) [YAML](https://eclipsefdn.github.io/api.eclipse.org-docs/openapi.yaml)

**Warning:** All above links are updated only after Travis CI finishes deployment

## Getting started
### Install

1. Install [Node JS](https://nodejs.org/)
2. Clone repo and run `npm install` in the repo root

### Usage

#### `npm start`
Starts the development server.

#### `npm run build`
Bundles the spec and prepares web_deploy folder with static assets.

#### `npm test`
Validates the spec.

#### `npm run gh-pages`
Deploys docs to GitHub Pages. You don't need to run it manually if you have Travis CI configured.

## Contributing

1. [Fork](https://gitlab.eclipse.org/help/user/project/repository/forking_workflow.md#creating-a-fork) the [eclipsefdn/it/api/api.eclipse.org-docs](https://gitlab.eclipse.org/eclipsefdn/it/api/api.eclipse.org-docs/-/tree/master) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_gitlab_username]/api.eclipse.org-docs.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/api/api.eclipse.org-docs/-/issues/new).

## Author

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://gitlab.eclipse.org/chrisguindon>

**Eric Poirier (Eclipse Foundation)**

- <https://twitter.com/ericpoir>
- <https://gitlab.eclipse.org/epoirier>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2018 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [api.eclipse.org-docs authors](https://gitlab.eclipse.org/eclipsefdn/it/api/api.eclipse.org-docs/-/graphs/master). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/api/api.eclipse.org-docs/-/blob/master/LICENSE).
